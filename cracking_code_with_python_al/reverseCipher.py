# Reverse Cipher (reverseCipher.py)
# @author = Purbayan Chowdhury

# message = "Three can keep a secret, if two of them are dead."
message = input("Enter message: ")
translated = ""

'''
i = len(message) - 1
while i >= 0:
    translated = translated + message[i]
    i = i-1

'''


def reverseCipher(msg):
    return msg[::-1]


translated = reverseCipher(message)

print(translated)
