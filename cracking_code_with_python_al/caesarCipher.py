# Caesar Cipher
# @author = Purbayan Chowdhury

import pyperclip

# message = '"You can show black is white by argument," said Filby, "but you will never convince me."'
# message = "gu5rrJpn1Jxrr3JnJ6rp5r7,JvsJ702J2sJ7urzJn5rJqrnqM"
# message = input("Enter message: ")

# Store the encrypted/decrypted form of the message:
# translated = ""

# # encryption/decryption key:
# key = 13

# # Whether the program encrypts or decrypts:
# # mode = 'encrypt'  # Set to either 'encrypt' or 'decrypt'.
# mode = 'decrypt'

# # Every possible symbol that can be encrypted:
# SYMBOLS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 !?.'
# # SYMBOLS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 !?.`~@#$%^&*()_+-=[]{}|;:<>,/'

# for symbol in message:
#     # Note: Only symbols in the SYMBOLS string can be encrypted/decrypted.
#     if symbol in SYMBOLS:
#         symbolIndex = SYMBOLS.find(symbol)

#         # Perform encryption/decryption:
#         if mode == 'encrypt':
#             translatedIndex = symbolIndex + key
#         elif mode == 'decrypt':
#             translatedIndex = symbolIndex - key

#         # Handle wraparound, if needed:
#         if translatedIndex >= len(SYMBOLS):
#             translatedIndex = translatedIndex - len(SYMBOLS)
#         elif translatedIndex < 0:
#             translatedIndex = translatedIndex + len(SYMBOLS)

#         translated = translated + SYMBOLS[translatedIndex]
#     else:
#         # Append the symbol without encrypting/decrypting:
#         translated = translated + symbol

# # Output the translated string:
# print(translated)
# pyperclip.copy(translated)


def caesarCipher(msg, key=3, mode="encrypt"):
    translated = ""
    translatedIndex = 0
    SYMBOLS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 !?.'
    for symbol in msg:
        # Note: Only symbols in the SYMBOLS string can be encrypted/decrypted.
        if symbol in SYMBOLS:
            symbolIndex = SYMBOLS.find(symbol)

            # Perform encryption/decryption:
            if mode == 'encrypt':
                translatedIndex = symbolIndex + key
            elif mode == 'decrypt':
                translatedIndex = symbolIndex - key

            # Handle wraparound, if needed:
            if translatedIndex >= len(SYMBOLS):
                translatedIndex = translatedIndex - len(SYMBOLS)
            elif translatedIndex < 0:
                translatedIndex = translatedIndex + len(SYMBOLS)

            translated = translated + SYMBOLS[translatedIndex]
        else:
            # Append the symbol without encrypting/decrypting:
            translated = translated + symbol

    return translated


translated = caesarCipher(
    'XCBSw88S18A1S 2SB41SE .8zSEwAS50D5A5x81V', 22, "decrypt")
print(translated)
